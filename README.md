# Task 1- triplet-sum-finder



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/kipkemoilvs/task-1-triplet-sum-finder.git
git branch -M main
git push -uf origin main
```
# Triplet Sum Finder

## Description
This repository contains a Python function (`find_triplets`) that takes a non-empty array of distinct integers and a target sum as input. The function finds and returns all unique triplets in the array whose sum is equal to the given target sum.

## Function Signature
```python
def find_triplets(array, target_sum):
    # Function implementation



# Example 1
array1 = [12, 3, 4, 1, 6, 9]
sum1 = 24
output1 = find_triplets(array1, sum1)
print("Input:", array1)
print("Output:", output1)

# Example 2
array2 = [1, 2, 3, 4, 5]
sum2 = 9
output2 = find_triplets(array2, sum2)
print("\nInput:", array2)
print("Output:", output2)
