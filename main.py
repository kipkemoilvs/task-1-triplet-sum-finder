def find_triplets(array, target_sum):
      # Create a set to store unique elements
  unique_elements = set()

  # Result list to store triplets
  result = []

  for i in range(len(array) - 2):
      current_sum = target_sum - array[i]
      # Check if current_sum can be obtained using two pointers
      seen = set()
      for j in range(i + 1, len(array)):
          complement = current_sum - array[j]
          if complement in seen:
              # Triplet found, add to the result
              result.append([array[i], array[j], complement])
          seen.add(array[j])
      unique_elements.add(array[i])

  return result

# Example usage:
array1 = [12, 3, 4, 1, 6, 9]
sum1 = 24
output1 = find_triplets(array1, sum1)
print("Input:", array1)
print("Output:", output1)

array2 = [1, 2, 3, 4, 5]
sum2 = 9
output2 = find_triplets(array2, sum2)
print("\nInput:", array2)
print("Output:", output2)
